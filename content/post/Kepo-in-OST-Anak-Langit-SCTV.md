---
title: "Kepo in OST Anak Langit SCTV"
post_highlight: ""
author: "KyataMedia" 
date: 2017-05-09T03:04:17Z
PublishDate: 2017-09-09T03T06:04:17Z
Lastmod: 2017-05-09T03:04:17Z
slug: "kepo-in-ost-anak-langit-sctv"
categories: [Entertainment]
tags: [Sinetron, TV, Entertainment, Youtube, Sejarah, Video, Berita, Biography, News]
cover: ""
draft: false
---

<b>Kepo in OST Anak Langit SCTV -,&nbsp;</b>buat kamu yang suka nonton Sinetron Anak Langit yang tayang di SCTV tiap sore pasti hafal sama lirik lagu yang jadi OST &nbsp;Anak langit, ya kan? tapi belum tentu tau asal muasal lagu tersebut. juga siapa penyanyi aslinya terlebih jika kamu terlahir sebagai generasi milenial yang lahir tahun 2000 an.<br />
<br />
mau tau? penasaran? berikut ini admin beri sedikit info mengenai lagu yang jadi <a href="http://kyatablog.blogspot.nl/2017/05/kepo-in-ost-anak-langit-sctv.html" target="_blank"><b>OST Anak Langit</b></a> di SCTV tersebut.


<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://upload.wikimedia.org/wikipedia/id/thumb/3/3a/Cover_Album_The_Best_of_Dewa_19.jpg/250px-Cover_Album_The_Best_of_Dewa_19.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img alt="sampul album The Best of Dewa 19 OST Anak Langit SCTV" border="0" height="312" src="https://upload.wikimedia.org/wikipedia/id/thumb/3/3a/Cover_Album_The_Best_of_Dewa_19.jpg/250px-Cover_Album_The_Best_of_Dewa_19.jpg" title="sampul album The Best of Dewa 19 OST Anak Langit SCTV" width="320" /></a></div>
<div class="separator" style="clear: both; text-align: center;">
<span style="color: blue;">Sampul album "The Best of Dewa 19"</span></div>
<div class="separator" style="clear: both; text-align: center;">
<span style="color: blue;"><br /></span></div>
<i style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;"><b>The Best of Dewa 19</b></i><span style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;">&nbsp;adalah album kumpulan karya-karya terbaik&nbsp;</span>Dewa 19<span style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;">&nbsp;yang dirilis pada tahun 1999. Album ini memuat lagu-lagu terbaik Dewa 19 semasa&nbsp;</span>Ari Lasso<span style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;">&nbsp;menjadi vokalis, sejak album pertama&nbsp;</span><i style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;">Dewa 19</i><span style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;">&nbsp;(1992) hingga&nbsp;</span><i style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;">Pandawa Lima</i><span style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;">&nbsp;(1997). Album ini memuat 2 buah lagu baru yaitu "Elang" dan "Persembahan Dari Surga". Album ini merupakan album perpisahan Dewa 19 dengan&nbsp;</span>Ari Lasso<span style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;">&nbsp;sebelum akhirnya digantikan oleh&nbsp;</span>Once<span style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;">. sumber :&nbsp;</span><span style="color: #222222; font-family: sans-serif;"><span style="font-size: 14px;"><a href="https://id.wikipedia.org/wiki/The_Best_of_Dewa_19?ref=kyatablog.blogspot.com" target="_blank">id.wikipedia.org</a></span></span><br />
<span style="background-color: white; color: #222222; font-family: sans-serif; font-size: 14px;"><br /></span>
<br />
<div style="text-align: left;">
<b>Artis/Band : Dewa 19&nbsp;</b></div>
<div style="text-align: left;">
<b>Judul lagu &nbsp;: Elang&nbsp;</b></div>
<div style="text-align: left;">
<b>Dirilis &nbsp; &nbsp; &nbsp; &nbsp; : November 1999&nbsp;</b></div>
<div style="text-align: left;">
<b>Album &nbsp; &nbsp; &nbsp; &nbsp; :&nbsp;<span style="text-align: center;">The Best of Dewa 19</span></b></div>
<div style="text-align: left;">
<span style="color: blue; text-align: center;"><br /></span></div>
<div style="text-align: left;">
lagu tersebut sebenarnya adalah lagu lawas dari Dewa 19 berjudul Elang yang release tahun 1999 an silam. nah loh.. sudah tua banget kan lagunya? bahkan mungkin banyak dari pembaca yang tahun pas release lagu tersebut masih belom pada lahir. :)&nbsp;</div>
<div style="text-align: center;">
<br /></div>
<b></b><br />
<div style="text-align: center;">
<b><b><span style="color: blue;">Dewa 19 - Elang (Original, Official Video)</span></b></b></div>
<b>
</b>
<div style="text-align: center;">
&nbsp;

<iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/M8XHE3CZYkU?rel=0&amp;showinfo=0" width="560"></iframe></div>
<div style="text-align: left;">
<div style="text-align: center;">
<br /></div>
<div style="text-align: left;">
lagu ini kembali populer setelah sekian lama redup dimakan zaman setelah Mahadewa meremake ulang lagu Elang dengan versi yang lebih modern. penulis sendiri kurang tau pasti kapan tepatnya. penulis mencoba mencari infonya di wikipedia tapi tidak penulis temukan info spesifik mengenai lagu elang yang versi Mahadewa.&nbsp;</div>
<div style="text-align: left;">
<br /></div>
<div style="text-align: left;">
di awal tahun 2017 an &nbsp;sinetron <a href="http://kyatablog.blogspot.nl/2017/05/daftar-isi-video-anak-langit.html" target="_blank"><b>Anak Langit</b></a> kembali mempopulerkan lagu ini. dan sampai hari dan masih cukup populer setelah dijadikannya lagu ini sebagai OST sinetronnya.</div>
<div style="text-align: left;">
<br /></div>
</div>
<div style="text-align: center;">
<h3 class="r" style="background-color: white; font-family: arial, sans-serif; font-size: 18px; font-weight: normal; margin: 0px; overflow: hidden; padding: 0px; text-align: center; text-overflow: ellipsis; white-space: nowrap;">
<span style="color: blue;">Mahadewa - Elang (Remake, OST. Anak Langit SCTV)</span></h3>
</div>
<div style="text-align: center;">
&nbsp;

<iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/KTb6G_D4CQM?rel=0&amp;showinfo=0" width="560"></iframe>&nbsp;</div>
<div style="text-align: center;">
<br /></div>

---
title: "Memfoto Layar Komputer Tanpa Software Tambahan"
post_highlight: ""
author: "KyataMedia" 
date: 2011-03-23T10:51:38Z
PublishDate: 2011-03-23T10:23:38Z
Lastmod: 2011-03-23T10:23:38Z
slug: "memfoto-layar-komputer-tanpa-software-tambahan"
categories: [Komputer]
tags: []
cover: "http://1.bp.blogspot.com/-pOce7LDm03M/TYkBh_ziaPI/AAAAAAAAACU/y0uGz-tGUBE/s1600/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B1.jpg"
draft: false
---

yup.. sebagai seorang bloger pastinya tak asing lagi yang namanya foto memfoto layar komputer kan? (so Pasti)<br />
<br />
Nah.. pada tips kali ini saya akan memberikan tips buat para bloger gimana caranya<br />
"Nemfoto Layar Komputer Tanpa Software Tambahan" sesuai dengan Judul Postingan di atas.yup dari pada Kebanyakan comel mending langsung aja ke TKP.. ( wah ko TKP?? Topik maksud ane..)<br />
Persiapan :<br />
- dah bawaan dari kompi semua sehh..<br />
- Tinggal siapin kopi+gula aja biar gak ngantuk pas baca ni tuto yang berbelit belit.kekekeke....<br />
langkah pertama ( 1 )<br />
Buka layar yang mau di ambil gambarnya.Sebagai contoh kita akan mengambil gambar dari Windows Media Player maka buka dulu Windows Media Player nya seperti pada ( contoh gambar 1 ) di bawah<br />
<div class="separator" style="clear: both; text-align: center;"><a href="http://1.bp.blogspot.com/-pOce7LDm03M/TYkBh_ziaPI/AAAAAAAAACU/y0uGz-tGUBE/s1600/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B1.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="240" src="http://1.bp.blogspot.com/-pOce7LDm03M/TYkBh_ziaPI/AAAAAAAAACU/y0uGz-tGUBE/s320/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B1.jpg" width="320" /></a></div>Gb.1<br />
Langkah Kedua ( 2 )<br />
<!--more--><br />
Tekan tombol  [ Print Screen Sys Rq ] pada Keyboard.atau kalo gak ada biasanya di singkat [ PSC ] karna tiap Layout Keyboard berbeda beda tergantung dari Jenis PC/Laptop/Notebook.<br />
liat gambar yang di tandai lingkaran warna merah pada gambar ( 2 ) di bawah<br />
Gb.2<br />
<div class="separator" style="clear: both; text-align: center;"><a href="http://3.bp.blogspot.com/-x_Do3hw9lJw/TYkG__ukp4I/AAAAAAAAACc/0A0yW6sq42s/s1600/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B2.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="240" src="http://3.bp.blogspot.com/-x_Do3hw9lJw/TYkG__ukp4I/AAAAAAAAACc/0A0yW6sq42s/s320/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B2.jpg" width="320" /></a></div>Gb.2<br />
oke... langkah selanjutnya alias ke ( 4 )<br />
Klik <br />
Start -&gt; All Programs -&gt;  Acessories -&gt;  Paint.<br />
liat gambar yang di tandai lingkaran warna merah pada gambar ( 4a ) di bawah<br />
<div class="separator" style="clear: both; text-align: center;"><a href="http://3.bp.blogspot.com/-m436QrzZnf8/TYkNSFQPOeI/AAAAAAAAACk/Bo4mFavyKJY/s1600/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B4a.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="240" src="http://3.bp.blogspot.com/-m436QrzZnf8/TYkNSFQPOeI/AAAAAAAAACk/Bo4mFavyKJY/s320/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B4a.jpg" width="320" /></a></div>Gb.4a<br />
setelah masuk pada menu Paint kemudian tinggal<br />
klik<br />
Edit -&gt; Paste<br />
liat gambar yang di tandai lingkaran warna merah pada gambar ( 4b ) di bawah<br />
<div class="separator" style="clear: both; text-align: center;"><a href="http://4.bp.blogspot.com/-4gq8LwW0N_w/TYkP5jR-xNI/AAAAAAAAACs/QNTwaKQMdLA/s1600/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B4b.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="240" src="http://4.bp.blogspot.com/-4gq8LwW0N_w/TYkP5jR-xNI/AAAAAAAAACs/QNTwaKQMdLA/s320/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B4b.jpg" width="320" /></a></div>Gb.4b<br />
Dan.... Tarang... gambar nya udah nongol tuh..<br />
nah dah nyampe langkah ( 5 )<br />
tinggal kita klik<br />
File -&gt; Save As<br />
liat gambar yang di tandai lingkaran warna merah pada gambar ( 5a ) di bawah<br />
<div class="separator" style="clear: both; text-align: center;"><a href="http://4.bp.blogspot.com/-oprDwnihNgE/TYkUdKTUW8I/AAAAAAAAAC0/Rxe0hPmkxFo/s1600/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B5a.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="240" src="http://4.bp.blogspot.com/-oprDwnihNgE/TYkUdKTUW8I/AAAAAAAAAC0/Rxe0hPmkxFo/s320/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B5a.jpg" width="320" /></a></div>G.b 5a<br />
selanjutnya pada kotak dialog Save As pilih Foldert tempat buat nyimpen gambarnya mau di direktori C atau E terserah Anda.<br />
langkah ke ( 6 )<br />
pada kolom File Name isikan nama gambar yang mau di simpan.Pada contoh saya beri nama FotoWMP.<br />
liat gambar yang di tandai lingkaran warna merah pada gambar ( 6a ) di bawah<br />
<div class="separator" style="clear: both; text-align: center;"><a href="http://2.bp.blogspot.com/-0vhLgSYw7dQ/TYkZ8nasUeI/AAAAAAAAAC8/M6cHNM8bH68/s1600/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B6a.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="240" src="http://2.bp.blogspot.com/-0vhLgSYw7dQ/TYkZ8nasUeI/AAAAAAAAAC8/M6cHNM8bH68/s320/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B6a.jpg" width="320" /></a></div>Gb. 6a<br />
pada kolom Save As Type pilih Format gambar yang mau anda simpan.sebagai contoh saya pilih Format JPG.<br />
liat gambar yang di tandai lingkaran warna merah pada gambar ( 6b ) di bawah<br />
<div class="separator" style="clear: both; text-align: center;"><a href="http://4.bp.blogspot.com/-mI7NWjYkalg/TYkd3HuAY7I/AAAAAAAAADE/Q_ks6ikzCQQ/s1600/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B6b.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="240" src="http://4.bp.blogspot.com/-mI7NWjYkalg/TYkd3HuAY7I/AAAAAAAAADE/Q_ks6ikzCQQ/s320/Memfoto%2BLayar%2BKomputer%2BTanpa%2BSoftware%2BTambahan%2B6b.jpg" width="320" /></a></div>Gb.6b<br />
nah tinggal Klik Tombol Save dan selesai deh....<br />
tinggal cari gambar yang telah anda buat tadi di folder yang telah anda pilih.gimana mudah bukan...?<br />
dah dulu ah..?? cape ngomong mulu.. kekekke<br />
Btw... lom abis kan.. kopi+gulanya???? sisain buat aku ya...??? kekekekke...
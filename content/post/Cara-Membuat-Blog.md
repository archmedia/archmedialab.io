---
title: "Cara Membuat Blog"
post_highlight: ""
author: "KyataMedia" 
date: 2010-06-13T14:31:00-07:00
PublishDate: 2010-06-13T14:31:00-07:00
Lastmod: 2010-06-13T14:31:00-07:00
slug: "cara-membuat-blog"
categories: [Guide]
tags: [Cara, Membuat Blog]
cover: ""
draft: false
---

<h2>Cara Membuat Blog, Cara buat Blog, Cara Bikin Blog, Cara Membuat Blog Mywapblog, Cara Membuat Blog Wordpress, Cara Membuat Blog Blogspot</h2><br /><img src="http://elmudunya.files.wordpress.com/2010/12/buku.jpg" alt="Cara Membuat Blog" /><br /><br /><b><span style="color:orange">DAFTAR ISI</span></b><br />
<br /><br /><br />
<br />
<h2>CARA MEMBUAT BLOGSPOT</h2><br />
<br />
<br />
<h2>CARA MEMBUAT WORDPRESS</h2><br />
belum ada hehe...<br />
<br />
<h2>CARA MEMBUAT MYWAPBLOG</h2><br />
<span style="color:orange">BAB 1. <a href="http://www.google.com/category/pendahuluan-3/1.xhtml"><b>PENDAHULUAN</b></a></span><br />
<br /><br /><br />
1.1.a <a href="http://www.google.com/mywapblog.xhtml">Sekilas Tentang MyWapBlog</a><br />
<br /><br /> <br />
1.1.b <a href="http://www.google.com/membuat-sebuah-wap-mobile-blog-gratis-de.xhtml">Membuat Sebuah Wap Mobile <br />
Blog Gratis Dengan MyWapBlog</a><br />
<br /><br /><br />
1.2 <a href="http://www.google.com/perbedaan-mywapblog-dan-website.xhtml"><b>Perbedaan Mywapblog Dan<br />
 Website</b></a> <b>New Update!</b><br />
<br /><br /><br />
1.3 <a href="http://www.google.com/tujuan-membuat-blog.xhtml">Tujuan Membuat Blog</a> <br />
<br /><br /><b><span style="color:orange">BAB 2. CARA DAFTAR MYWAPBLOG</span></b><br />
<br /><br /><br />
2.1 <a href="http://www.google.com/cara-membuat-blog-di-mywapblog.xhtml">Cara Membuat Blog Di<br />
Mywapblog</a><br />
<br /><br /><b><span style="color:orange">BAB 3. LOGIN ADMIN MYWAPBLOG</span></b><br />
<br /><br /><b><span style="color:orange">BAB 4. SETTING MYWAPBLOG</span></b><br />
<br /><br /><br />
4.1 Settings Blog <b>(in progress...)</b><br />
<br /><br /><br />
4.2 Settings Display <b>(in progress...)</b><br />
<br /><br /><br />
4.3 Settings Personal <b>(in progress...)</b><br />
<br /><br /><br />
4.4 Settings Misc <b>(in progress...)</b><br />
<br /><br /><br />
4.5 Setting Password <b>(in progress...)</b><br />
<br /><br /><b><span style="color:orange">BAB 5. MENGELOLA BLOG</span></b><br />
<br /><br /><br />
5.1 <a href="http://www.google.com/cara-membuat-postingan-pada-mywapblog.xhtml">Cara Membuat Postingan Pada<br />
Mywapblog</a> <b>Bagi Pemula</b><br />
<br /><br /><br />
5.2 Cara Mengatur Format Huruf <b>(in progress...)</b><br />
<br /><br /><br />
5.3 <a href="http://www.google.com/menghilangkan-readmore-pada-judul-postin.xhtml">Menghilangkan Readmore Pada<br />
 Judul Postingan ke 2 dan<br />
 Seterusnya</a><br />
<br /><br /><br />
5.4 Cara Mengganti Tulisan Readmore <b>(in progress...)</b><br />
<br /><br /><br />
5.5 <a href="http://www.google.com/cara-menyisipkan-gambar-pada-postingan-d.xhtml">Cara Menambah Gambar Di Dalam Artikel</a> <br />
<br /><br /><br />
5.6 Cara Menambah Video <b>(in progress...)</b><br />
<br /><br /><br />
5.7 Menentukan Kategori dan Subkategori <b>(in progress...)</b><br />
<br /><br /><br />
5.8 Membuat Artikel Selalu Tampil Di Halaman Utama<br />
(Sticky Post) <b>(in progress...)</b><br />
<br /><br /><br />
5.9. <a href="http://www.google.com/cara-membuat-artikel-selalu-tampil-di-ha.xhtml">Cara Membuat Artikel Selalu<br />
Tampil Di Halaman Utama (Sticky<br />
Post) Pada Mywapblog</a><br />
<br /><br /><br />
5.10 Mengelola Komentar <b>(in progress...)</b><br />
<br /><br /><br />
5.11 Mengelola Link/Blogroll <b>(in progress...)</b><br />
<br /><br /><br />
7.12 <a href="http://www.google.com/cara-mengatur-ukuran-gambar-manual.xhtml">Cara Mengatur Ukuran <br />
Gambar Manual</a><br />
<br /><br /><br />
7.13 <a href="http://www.google.com/cara-membuat-link-shortcut-go-to-blogrol.xhtml">Cara Membuat Link Shortcut Go<br />
 To Blogroll Pada Postingan</a><br />
<br /><br /><br />
7.14 <a href="http://www.google.com/cara-membuat-link-shortcut-go-to-comment.xhtml">Cara Membuat Link Shortcut Go <br />
To Comments Pada Postingan</a><br />
<br /><br /><br />
7.15 <a href="http://www.google.com/cara-membuat-garis-sekat-pada-postingan.xhtml">Cara Membuat Garis Sekat Pada <br />
Postingan Mywapblog</a><br />
<br /><br /><br />
7.16 <a href="http://www.google.com/cara-memasang-kode-html-dan-bb-code-pada.xhtml">Cara Memasang Kode HTML Dan<br />
 BB CODE Pada Navigasi Blog <br />
Mywapblog</a><b>Wajib Dibaca Bagi Pemula</b><br />
<br /><br /><b><span style="color:orange">BAB 6. MENGELOLA APPEARANCE / TAMPILAN BLOG</span></b> <br />
<br /><br /><br />
6.1 Menginstall Theme / Mengganti Themes <b>(in progress...)</b><br />
<br /><br /><br />
6.2 <a href="http://www.google.com/cara-mengganti-themecss-versi-desktop-pa.xhtml">Cara Mengganti Theme/CSS Versi<br />
Desktop Pada Mywapblog</a><br />
<br /><br /><br />
6.3 <a href="http://www.google.com/cara-mengatur-menu-category-di-atas-post.xhtml"><br />
Cara Mengatur Menu<br />
 Category Di Atas Postingan<br />
 Menjadi Di Bawah Menu <br />
Navigasi Pada Mywapblog</a><br />
<br /><br /><br />
6.4 <a href="http://www.google.com/cara-mengganti-favicon-pada-mywapblog.xhtml"><br />
Cara Mengganti Favicon Pada <br />
Mywapblog</a><br />
<br /><br /><b><span style="color:orange">BAB 7. MEMPERCANTIK<br />
MYWAPBLOG</span></b> <br />
<br /><br /><br />
7.1 <a href="http://www.google.com/cara-memasang-widget-google-pagerank-pad.xhtml">Cara Memasang Widget Google<br />
 Pagerank Pada Mywapblog</a> <b>New Update!</b><br />
<br /><br /><br />
7.2 <a href="http://www.google.com/cara-memasang-widget-alexa-rank-pada-myw.xhtml">Cara Memasang Widget Alexa<br />
Rank Pada Mywapblog</a><br />
<br /><br /><br />
7.3 <a href="http://www.google.com/cara-memasang-flag-counter-pada-mywapblo.xhtml">Cara Memasang Flag Counter <br />
Pada Mywapblog</a> <b>NEW UPDATE!</b><br />
<br /><br /><br />
7.4. Membuat dan Menginstall Feed Burner <b>(in progress...)</b><br />
<br /><br /><br />
7.5 Menginstall Visitor Counter Histats <b>(in progress...)</b><br />
<br /><br /><br />
7.6 Menginstall Status Yahoo! Messenger <b>(in progress...)</b><br />
<br /><br /><br />
7.7 <a href="http://www.google.com/cara-memasang-emoticon-kaskus-pada-mywap.xhtml">Cara Memasang Emoticon Kaskus Pada Mywapblog</a><br />
<br /><br /><br />
7.8 <a href="http://www.google.com/cara-memasang-google-translate-pada-mywa.xhtml">Cara Memasang Google Translate Pada Mywapblog</a><br />
<br /><br /><br />
7.9 <a href="http://www.google.com/cara-memasang-google-search-pada-mwb-myw.xhtml">Cara Memasang Google Search Pada MWB (Mywapblog)</a><br />
<br /><br /><br />
7.10 <a href="http://www.google.com/cara-memasang-grafik-pageview-pada-navig.xhtml">Cara Memasang Grafik Pageview Pada Navigasi Blog</a><br />
<br /><br /><br />
7.11 <a href="http://www.google.com/cara-mempercantik-blog-mywapblog-dengan.xhtml">Cara Mempercantik Blog<br />
Mywapblog Dengan Tabel Portal</a><br />
<br /><br /><br />
7.12 <a href="http://www.google.com/mwb-plug-n-play-widget.xhtml">MWB Plug N Play Widget</a><br />
<br /><br /><br />
7.13 <a href="http://www.google.com/cara-memasang-tombol-sms-me-pada-mywapbl.xhtml">Cara Memasang Tombol SMS ME Pada Mywapblog</a> <b>NEW UPDATE!</b><br />
<br /><br /><br />
7.14 <a href="http://www.google.com/cara-membuat-link-download-dengan-gambar.xhtml">Cara Membuat Link Download<br />
 Dengan Gambar Pada Mywapblog</a> <b>NEW UPDATE!</b><br />
<br /><br /><br />
7.15 <a href="http://www.google.com/cara-memasang-quick-counter-pada-mywapbl.xhtml">Cara Memasang Quick Counter <br />
Pada Mywapblog</a> <b>NEW UPDATE!</b><br />
<br /><br /><br />
7.16 <a href="http://www.google.com/cara-memasang-random-al-quran-dictionary.xhtml">Cara Memasang Random Al-<br />
Qur'an Dictionary Pada <br />
Mywapblog</a> <b>NEW UPDATE!</b><br />
<br /><br /><br />
7.17 <a href="http://www.google.com/cara-membuat-link-dengan-tombol-buton-pa.xhtml">Cara Membuat Link Dengan<br />
 Tombol / Buton Pada Mywapblog</a> <b>NEW UPDATE!</b><br />
<br /><br /><br />
7.18 <a href="http://www.google.com/cara-memasang-kalender-hijriah-pada-mywa.xhtml">Cara Memasang Kalender Hijriah <br />
Pada Mywapblog</a> <b>NEW UPDATE!</b><br />
<br /><br /><br />
7.19 <a href="http://www.google.com/cara-memasang-widget-ip-adress-checker-p.xhtml">Cara Memasang Widget IP<br />
 Adress Checker Pada<br />
 Mywapblog</a> <b>NEW UPDATE!</b><br />
<br /><br /><b><span style="color:orange">BAB 8. MYWAPBLOG MONETIZING BLOG</span></b><br />
<br /><br /><br />
8.1 Cara Medaftar Acount Publisher Buzzcity <b>(in progress...)</b><br />
<br /><br /><br />
8.2 <a href="http://www.google.com/cara-memasang-iklan-buzzcity-di-mywapblo.xhtml">Cara Memasang Iklan Buzzcity Di Mywapblog</a><br />
<br /><br /><br />
8.3 Cara Curang Meningkatkan Click Buzzcity <b>(in progress...)</b><br />
<br /><br /><b><span style="color:orange">BAB. 9 OTHER MYWAPBLOG TIPS N TRICK</span></b><br />
<br /><br /><br />
9.1 <a href="http://www.google.com/cara-mengintip-guestbook-mwb-yang-di-hap.xhtml">Cara Mengintip Guestbook MWB Yang Dihapus</a><br />
<br /><br /><br />
9.2 <a href="http://www.google.com/trik-manipulasi-visitor-counter-pada-foo.xhtml">Trik Manipulasi Visitor Counter<br />
Pada Footer Mwb</a><br />
<br /><br /><br />
9.3 <a href="http://www.google.com/cara-buka-mywapblog-versi-wap-mobile-lew.xhtml">Cara Buka Mywapblog Versi WAP<br />
Mobile Lewat PC</a><br />
<br /><br /><br />
9.4 <a href="http://www.google.com/cara-mengalihkan-wap-xtgem-ke-mywapblog.xhtml">Cara Mengalihkan Wap Xtgem Ke <br />
Mywapblog Dengan Meta Refresh</a><br />
<br /><br /><br />
9.5 <a href="http://www.google.com/cara-mengintip-css-mywapblog-new-version.xhtml">Cara Mengintip CSS Mywapblog <br />
New Version</a><br />
<br /><br /><br />
9.6 <a href="http://www.google.com/trik-melihat-content-blog-yang-di-hiden.xhtml">Trik Melihat Content Blog<br />
Yang Di Hiden Pake CSS</a><br />
<br /><br /><br />
9.7 <a href="http://www.google.com/kumpulan-favicon.xhtml">Kumpulan Favicon</a><br />
<br /><br /><b><span style="color:orange">BAB. 10 MYWAPBLOG TOOLS</span></b><br />
<br /><br /><br />
10.1 <a href="http://www.google.com/css-creator-aplikasi-css-maker-java-vers.xhtml">CSS Creator Aplikasi CSS<br />
 Maker Java Version</a><br />
<br /><br /><br />
10.2 <a href="http://www.google.com/mudahnya-belajar-dan-membuat-css-mwb-den.xhtml">Mudahnya Belajar dan <br />
Membuat CSS Mwb Dengan <br />
Aplikasi Css Maker</a><br />
<br /><br /><b><span style="color:orange">BAB. 11 MYWAPBLOG CSS COLECTION</span></b><br />
<br /><br /><br />
11.1 <a href="http://www.google.com/share-css-lunatic-touch-by-mujab-dot-net.xhtml">Share Css Lunatic Touch By Mujab Dot Net</a><br />
<br /><br /><br />
11.2 <a href="http://www.google.com/css-facebook-clone-request.xhtml">CSS Facebook Clone Request</a><br />
<br /><br /><br />
11.3 <a href="http://www.google.com/share-css-violete-up-grade-dari-css-up-f.xhtml">Share CSS Violete | Up Grade<br />
Dari CSS Up Facebook Flambon</a><br />
<br /><br /><b><span style="color:orange">BAB. 12 BELAJAR HTML DAN BB CODE</span></b><br />
<br /><br /><br />
12.1 <a href="http://www.google.com/cara-membuat-text-area-pada-mwb.xhtml">Cara Membuat Text Area Pada Mwb</a> <b>Part 1</b> <b>(Masih Dalam Perbaikan)</b><br />
<br /><br /><br />
12.2 <a href="http://www.google.com/cara-membuat-text-area.xhtml">Cara Membuat Text Area</a> <b>Part 2</b><br />
<br /><br /><br />
12.3 <a href="http://www.google.com/cara-menyisipkan-underlinegaris-bawah-pa.xhtml">CARA MENYISIPKAN UNDERLINE/<br />
GARIS BAWAH PADA POSTINGAN<br />
DENGAN BB CODE</a><br />
<br /><br /><br />
12.4 <a href="http://www.google.com/kumpulan-bb-code-cara-penulisannya-beser.xhtml">Kumpulan BB CODE Cara<br />
 Penulisannya Beserta Contohnya</a><br />
<br /><br /><br />
12.5 <a href="http://www.google.com/cara-membuat-text-miring.xhtml">Cara Membuat Text Miring<br />
</a><br />
<br /><br /><br />
12.6 <a href="http://www.google.com/cara-membuat-text-berkedipblink.xhtml">Cara Membuat Text Berkedip/<br />
Blink</a><br />
<br /><br /><br />
12.7 <a href="http://www.google.com/cara-membuat-text-berjalan.xhtml">Cara Membuat Text Berjalan</a><br />
<br /><br /><a href=""></a><br />
<br /><br /><br />
12.8 <a href="http://www.google.com/cara-membuat-link-nofollow.xhtml">Cara Membuat Link NoFollow</a><br />
<br /><br /><br />
12.9 <a href="http://www.google.com/cara-membuat-link-auto-windowotomatis-te.xhtml">Cara Membuat Link Auto<br />
Window/Otomatis Terbuka di <br />
New Tab</a><br />
<br /><br /><b><span style="color:orange">BAB. 13 TIPS N TRICK CSS MYWAPBLOG</span></b><br />
<br /><br /><br />
12.1 <a href="http://www.google.com/script-random-css-mywapblog.xhtml">Script Random CSS Mywapblog</a><br />
<br /><br /><br />
12.2 <a href="http://www.google.com/bikin-blog-ringan-dengan-css-compressor.xhtml">Tentang CSS Compressor Dan<br />
Cara Kerjanya</a><br />
<br /><br /><br />
12.2 <a href="http://www.google.com/cara-mengatur-lebar-gambar-pada-mwb.xhtml">Cara Mengatur Lebar Gambar<br />
Pada MWB</a><br />
<br /><br /><br />
12.3 <a href="http://www.google.com/random-css-3-in-1-mwb-touch-css-style-im.xhtml">Random Css 3 In 1 Mwb Touch <br />
Css Style Import</a><br />
<br /><br /><br />
12.4 <a href="http://www.google.com/tutorial-css-terlengkap-by-abe-poetra-pd.xhtml">Tutorial CSS Terlengkap by <br />
Abe Poetra (pdf)</a><br />
<br /><br /><b><span style="color:orange">BAB. 14 MYWAPBLOG SEARCH ENGINE OPTIMIZATION (SEO)</span></b><br />
<br /><br /><br />
14.1 <a href="http://www.google.com/cara-membuat-meta-tags-pada-blog-mwb-ses-2.xhtml">Cara Membuat Meta Tags<br />
 Pada Blog MWB Sesuai Dengan<br />
 Pedoman Webmaster Agar<br />
 Lebih SEO Friendly</a><br />
<br /><br /><br />
14.2 <a href="http://www.google.com/rss-xml-alternatif-sitemap-xml-pada-goog.xhtml"><br />
RSS.XML ALTERNATIF<br />
SITEMAP.XML PADA GOOGLE<br />
WEBMASTER</a><br />
<br /><br /><br />
14.3 <a href="http://www.google.com/cara-mencari-keyword-dan-topik-yang-pali.xhtml">Cara Mencari Keyword dan Topik<br />
 Yang Paling Banyak Dicari Orang</a><br />
<br /><br /><br />
14.4 <a href="http://www.google.com/serp-tool-checker.xhtml">Cara Mudah Cek Posisi SERP<br />
 Blog Dengan SERP Checker<br />
Tool</a><br />
<br /><br /><br />
14.5 <a href="http://www.google.com/cara-verifikasimendaftar-blog-mywapblog.xhtml">Cara Verifikasi/Mendaftar Blog<br />
 Mywapblog Ke Google<br />
 Webmaster</a><br />
<br /><br /><br />
14.6 <a href="http://www.google.com/cara-membuat-artikel-selalu-tampil-di-ha-2.xhtml">Cara Membuat Artikel Selalu<br />
Tampil Di Halaman Utama Pada<br />
Mywapblog ( Dampak pada<br />
Search Engine/SEO )</a><br />
<br /><br /><br />
14.7 <a href="http://www.google.com/apa-itu-ping-fungsi-ping-manfaat-ping-da.xhtml">Apa Itu Ping Fungsi Ping Manfaat <br />
Ping Dan Cara Memasang Ping O <br />
Matic Pada Mywapblog</a><br />
<br /><br /><b><span style="color:orange">BAB. 14 ALL ABOUT MYWAPBLOG PAGERANK</span></b><br />
<br /><br /><br />
14.1 <a href="http://www.google.com/manfaat-menggunakan-atribut-nofollow-pad.xhtml">Manfaat Menggunakan Atribut <br />
Nofollow Pada Link Keluar</a><br />
<br /><br /><br />
14.2 <a href="http://www.google.com/rumus-pagerank.xhtml">RUMUS PAGERANK</a><br />
<br /><br /><br />
14.3 <a href="http://www.google.com/cara-meningkatkan-pagerank.xhtml">CARA MENINGKATKAN PAGERANK</a><br />
<br /><br /><b><span style="color:orange">BAB. 15 ALL ABOUT MYWAPBLOG ALEXA RANK</span></b><br />
<br /><br /><br />
15.1 Diet Miss Alexa <b>(18+ Only wkwk justkiding)</b><br />
<br /><br /><br />
15.2 <a href="http://www.google.com/cara-meningkatkan-jumlah-backlink-dan-si.xhtml">Cara Meningkatkan Jumlah <br />
Backlink Dan Sites Link in Alexa<br />
 Rank Dengan Cepat</a><br />
<br /><br />
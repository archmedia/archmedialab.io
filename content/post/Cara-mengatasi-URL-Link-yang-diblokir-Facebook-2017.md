---
title: "Cara Mengatasi URL Link Yang Diblokir Facebook 2017"
post_highlight: ""
author: "KyataMedia" 
date: 2017-04-16T08:04:17Z
PublishDate: 2017-08-16T06:04:17Z
Lastmod: 2017-04-16T08:04:17Z
slug: "cara-mengatasi-url-link-yang-diblokir-facebook-201"
categories: [Guide]
tags: [Blog, Tutorial, Blogspot, Blogger, Panduan, Tips dan Trick, Internet, Social Media, Facebook]
cover: "https://1.bp.blogspot.com/-nm1b-8t-nco/WPOB5BHRf2I/AAAAAAAAAY0/N5E78lK7AXkSjrcGtjGTT5m4k8ixvVknACLcB/s1600/caranotepad1.jpg" 
draft: false
---
<b>Cara mengatasi URL/Link yang diblokir Facebook ,-</b> Jika URL/link website anda diblok facebook namun anda ingin tetap bisa share atau mempostingnya di facebook chat/group atau halaman anda? anda masuk di postingan yang tepat karena saya disini mau membagikan trik nya, agar tetap bisa sharing link website kapan saja tanpa beresiko lagi website anda diblok lagi.<br />
<br />
sebelumnya &nbsp;saya sudah googling sana-sini buat nyari &nbsp;cara agar tetap bisa share atau posting URL/link yang sudah di blok <b><a href="https://facebook.com/" target="_blank">facebook</a></b>, namun yang didapat kebanyakan sudah ga work. ada juga yang jelasin cara banding. kenapa judulnya nggak cara banding url yang di block facebook aja? mau banding sampai ubanan juga kalo linknya mengarah ke konten ga bener ya kagak bakalan diterima bandingnya. jadi, masalah banding menurut saya bukan solusi karena kemungkinan disetujui bandingnnya sangat kecil, kecuali diblok nya karena over posting.<br />
<br />
tentu saja saya sharing trik bukan dengan asumsi URL website anda diblok karena over posting, tapi diblok karena banyak hal yang melanggar TOS Facebook.<br />
<br />
beberapa cara yang udah saya coba dan udah ga work sekarang (tahun 2017) karena mesin deteksi spam facebook sudah sangat canggih diantaranya:<br />
<br />
- pake script PHP dengan 301 redirect<br />
- pake link shortener dengan metode 301 redirect seperti goo.gl.<br />
- pake link shortener yang pake skip ads semacem adf.ly atau short.es. karena adf.ly sama shrt.es nya juga udah diblok atau masuk list situs berbahaya (lagian kalopun jadi sapa juga yang mau ngeklik link buat nyari recehan di FB)<br />
<br />
bahkan sampai cara yang lumayan ekstrim dengan jawascript windows location dengan delay waktu redirect ke situs tujuan 5 detik juga udah ga bisa. namun akan tetap saya masukan di ahir postingan ini sekalipun saya mencoba udah ga bisa.<br />
<br />
<b>Cara posting url/link situt/blog ke facebook 2017</b><br />
<br />
sekarang facebook sudah sangat ketat memberantas situs spam dan situs yang dianggap tidak aman /berbahaya bagi pengguna facebook dikarenakan jumlahnya yang terus meningkat dari hari ke hari. pada postingan ini saya akan membagikan cara yang lumayan unik dan agak maksa tapi saya jamin work 100% setidaknya pada tahun 2017. dan berikut ini caranya.<br />
<br />
<b>Dengan memakai layanan online notepad dari anotepad.com</b><br />
<br />
mungkin cara ini agak mirip dengan cara unblock url facebook dengan situs bookmark seperti lintas.me atau bloggue.com hanya saja lebih sederhana karena tidak perlu login dulu apalagi daftar, tidak perlu memposting sebagian isi artikel anda, tapi cukup hanya dengan menuliskan judul artikel dan menyalink URL postingan saja.<br />
<br />
pertama anda kunjungi dulu situsnya&nbsp;<a href="https://anotepad.com/"><b>https://anotepad.com/</b></a><br />
<br />
lalu klik <span style="color: blue;"><b>Enable Rich-Text Editor</b></span><br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-nm1b-8t-nco/WPOB5BHRf2I/AAAAAAAAAY0/N5E78lK7AXkSjrcGtjGTT5m4k8ixvVknACLcB/s1600/caranotepad1.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://1.bp.blogspot.com/-nm1b-8t-nco/WPOB5BHRf2I/AAAAAAAAAY0/N5E78lK7AXkSjrcGtjGTT5m4k8ixvVknACLcB/s1600/caranotepad1.jpg" /></a></div>
<br />
<br />
isikan judul pada judul, isikan pesan singkat pada kolom (tidak wajib) dilanjutkan klik icon link (gambar rantai)<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-2X9C3JC8vqg/WPOB4SGXS4I/AAAAAAAAAYs/kbo5cGx_8Lgy5FIZKxlVlgnoZdEbQqYkQCLcB/s1600/caranotepad2.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://1.bp.blogspot.com/-2X9C3JC8vqg/WPOB4SGXS4I/AAAAAAAAAYs/kbo5cGx_8Lgy5FIZKxlVlgnoZdEbQqYkQCLcB/s1600/caranotepad2.jpg" /></a></div>
<br />
masukan link pada link, isi Text to display bisa dengan judul artikel atau nama situs/blog anda, title isikan title artikel atau nama situs/blog anda, lanjukan klik tombol <b><span style="color: blue;">OK</span></b><br />
<div class="separator" style="clear: both; text-align: center;">
<br /></div>
<div class="separator" style="clear: both; text-align: center;">
<a href="https://4.bp.blogspot.com/-Wd-BAtiznMY/WPOB4hLiCUI/AAAAAAAAAYw/FfqLUr1VWfkElP9pdzNBkdXCF1QAGKF6wCLcB/s1600/caranotepad3.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://4.bp.blogspot.com/-Wd-BAtiznMY/WPOB4hLiCUI/AAAAAAAAAYw/FfqLUr1VWfkElP9pdzNBkdXCF1QAGKF6wCLcB/s1600/caranotepad3.jpg" /></a></div>
<div class="separator" style="clear: both; text-align: center;">
<br /></div>
&nbsp;pastikan semua kolom sudah diisi dengan benar. kalo sudah, klik tombol <b><span style="color: blue;">Save</span></b><br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://1.bp.blogspot.com/-P2T6v4l9i-c/WPOB5wPPo3I/AAAAAAAAAY4/2pqve8YQy_AG0BulqZgnQ2X9TpCtcyaigCLcB/s1600/caranotepad4.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://1.bp.blogspot.com/-P2T6v4l9i-c/WPOB5wPPo3I/AAAAAAAAAY4/2pqve8YQy_AG0BulqZgnQ2X9TpCtcyaigCLcB/s1600/caranotepad4.jpg" /></a></div>
<br />
kalo sudah ada peringatan notead tersimpan dengan backround warna biru, lanjutkan klik link <b><span style="color: blue;">Preview</span></b> untuk melihat hasilnya<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-yXntidmOtq8/WPOB63jUijI/AAAAAAAAAY8/GjmE2oepcWMDy2-V6QOx5vKsVhDygXLMgCLcB/s1600/caranotepad5.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-yXntidmOtq8/WPOB63jUijI/AAAAAAAAAY8/GjmE2oepcWMDy2-V6QOx5vKsVhDygXLMgCLcB/s1600/caranotepad5.jpg" /></a></div>
<br />
hasilnya nanti yang klik link/url yang anda share di Facebook akan seperi yang ada pada kotak besar.sedang pada kotak kecil adalah URL notepad yang tadi anda buat yang digunakan untuk share ke FB. silahkan simpan URL-nya.<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-EHZPggUG-Hg/WPOB7iQPfOI/AAAAAAAAAZA/v-IVN3Nwjt8P0EsUSmE5kN0OpiPJxb3AACLcB/s1600/caranotepad6.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-EHZPggUG-Hg/WPOB7iQPfOI/AAAAAAAAAZA/v-IVN3Nwjt8P0EsUSmE5kN0OpiPJxb3AACLcB/s1600/caranotepad6.jpg" /></a></div>
<br />
<br />
sebagai catatan, jika anda mau bebas mengedit catatan anda kapanpun dan dimanapun maka sebelum membuat catatan pastikan anda sudah mendaftar jadi member dan sudah log in. kalo tidak mendaftar anda tidak bisa mengedit catatan anda lagi kalo cookes pada browser anda sudah anda reset ulang atau cookies sudah kadaluarsa.<br />
<br />
url edit notepadnya itu hampir sama dengan url yang buat share ke FB hanya dihilangkan pada readnya aja. &nbsp;seperi ini contohnya:<br />
<br />
buat baca =&gt; <span style="color: blue;">https://anotepad.com/note/read/rx295c</span><br />
buat edit =&gt; <span style="color: blue;">https://anotepad.com/notes/rx295c</span><br />
<br />
selain mudah, simple dan ga perlu daftar, anda juga bisa mendapatkan backlink website/blog permanen yang berkualitsa dan tentu saja gratis.<br />
<br />
<b>Trik unblock Tautan/link/url yang diblokir Facebook pake Javascript Windows Location</b><br />
<span style="color: red;">(Cara alternative yang saya sendiri gagal)</span><br />
<span style="color: red;"><br /></span>
untuk cara yang kedua ini selain agak ribet juga saya mencobanya tetap aja kedetek sama Facebook situs tujuannya. tapi dicoba aja sapa tau anda bisa pake cara ini.<br />
<br />
pertama log in dulu di blogger seperti biasa. kalo anda tidak punya akun blogger daftar dulu di <a href="http://www.blogger.com/"><b>www.blogger.com</b></a><br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://4.bp.blogspot.com/-HJe_YCPYBUM/WPOH5QopK4I/AAAAAAAAAZU/CjpO7OQqk-MkRaFYt0dCEkt7H18hR0pHwCLcB/s1600/cara1a.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="400" src="https://4.bp.blogspot.com/-HJe_YCPYBUM/WPOH5QopK4I/AAAAAAAAAZU/CjpO7OQqk-MkRaFYt0dCEkt7H18hR0pHwCLcB/s400/cara1a.jpg" width="357" /></a></div>
<br />
klik panah arah bawah di sebelah kanan Dashboard, klik <b><span style="color: blue;">New Blog</span></b><br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://4.bp.blogspot.com/-24yEIwICU3k/WPOH5CZvwsI/AAAAAAAAAZQ/ph_LWZ5-ocIZNEbIouHgzgbtF6gzZJJpgCLcB/s1600/cara1.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://4.bp.blogspot.com/-24yEIwICU3k/WPOH5CZvwsI/AAAAAAAAAZQ/ph_LWZ5-ocIZNEbIouHgzgbtF6gzZJJpgCLcB/s1600/cara1.jpg" /></a></div>
&nbsp;isikan Title pada Title, masukan url yang anda inginkan dan tunggu masih tersedia apa tidak. kalo tersedia, lanjutkan klik tema simple (biar simpe aja editnya) dan dilanjut dengan klik tombol Create Blog<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-Jth8XjYerNk/WPOH5xKnP4I/AAAAAAAAAZY/w9KAX6_dypw3zkBeuACnwRr0n5pw3pO3ACLcB/s1600/cara2.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-Jth8XjYerNk/WPOH5xKnP4I/AAAAAAAAAZY/w9KAX6_dypw3zkBeuACnwRr0n5pw3pO3ACLcB/s1600/cara2.jpg" /></a></div>
<br />
<br />
ini yang penting, setting privasi ke no semua dengan cara klik menu setting &gt; basic<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-tGxuEmcLy7c/WPOH7YAgYYI/AAAAAAAAAZg/iqhHMvpMybEhWh3aXfQ2QSRJQWe13oJuQCLcB/s1600/cara3.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-tGxuEmcLy7c/WPOH7YAgYYI/AAAAAAAAAZg/iqhHMvpMybEhWh3aXfQ2QSRJQWe13oJuQCLcB/s1600/cara3.jpg" /></a></div>
<br />
<br />
lalu klik Setting pada dashboard dilanjutkan klik tombol gerigi<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-iw8xZIuDhHk/WPOH7O2qQaI/AAAAAAAAAZc/CEFuCU3FgKUavuTA5dhodgJ2h5MIAfOZACLcB/s1600/cara4.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-iw8xZIuDhHk/WPOH7O2qQaI/AAAAAAAAAZc/CEFuCU3FgKUavuTA5dhodgJ2h5MIAfOZACLcB/s1600/cara4.jpg" /></a></div>
<br />
<br />
<br />
pilih pada pilihan paling bawah, <b><span style="color: blue;">No, Show desktop theme on mobile devices</span></b>.<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://4.bp.blogspot.com/-fwSVkHonJOw/WPOH74-CM9I/AAAAAAAAAZk/EIFHqAPH_Y0OBvDvD_v_VclToR28NrAxACLcB/s1600/cara5.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://4.bp.blogspot.com/-fwSVkHonJOw/WPOH74-CM9I/AAAAAAAAAZk/EIFHqAPH_Y0OBvDvD_v_VclToR28NrAxACLcB/s1600/cara5.jpg" /></a></div>
<br />
<br />
geser kebawah, cari dan klik pada menu <b><span style="color: blue;">Revert to classic themes</span></b><br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://4.bp.blogspot.com/-V0xLmTYWu0g/WPOH8QzfofI/AAAAAAAAAZo/gODK86ehgSw0S238AIculdiKVB2FfUFYwCLcB/s1600/cara6.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://4.bp.blogspot.com/-V0xLmTYWu0g/WPOH8QzfofI/AAAAAAAAAZo/gODK86ehgSw0S238AIculdiKVB2FfUFYwCLcB/s1600/cara6.jpg" /></a></div>
<br />
<br />
lalu hapus semua sampai kosong, ganti dengan code dibawah ini<br />
<br />
<blockquote class="tr_bq">
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;!DOCTYPE html&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;head&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;meta http-equiv="content-type" content="text/html;charset=utf-8"&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;meta name="referrer" content="none"&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;meta name="robots" content="noindex,nofollow,noodp,noydir"/&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;meta property="og:locale" content="en_US" /&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;meta property="og:type" content="article" /&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;meta property="og:title" content="Masukan judul halaman disini" /&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;meta property="og:description" content="Masukan deskripsi halaman disini" /&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;meta property="og:url" content="http://testlinkfb1.blogspot.com" /&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;meta property="og:image" content="http://contoh.com/logo.png" /&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;script type="text/javascript"&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">function Redirect()</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">{</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&nbsp;window.location="http://www.url-yang-di-blokir.com";</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">}</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">setTimeout('Redirect()', 5);</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;/script&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;title&gt;Memuat halaman...&lt;/title&gt;</span></blockquote>
<blockquote class="tr_bq">
<span style="color: blue; font-size: x-small;">&lt;/head&gt;&lt;body&gt;&lt;/body&gt;&lt;/html&gt;</span></blockquote>
</blockquote>
<br />
dan dilanjutkan dengan klik tombol &nbsp;<b><span style="color: blue;">Save theme</span></b><br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://4.bp.blogspot.com/-CWfRG6QQJNw/WPOH9Ybar5I/AAAAAAAAAZs/anacTpv0H3M1_bH9GG1br6-XmYsBFuJAgCLcB/s1600/cara7.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://4.bp.blogspot.com/-CWfRG6QQJNw/WPOH9Ybar5I/AAAAAAAAAZs/anacTpv0H3M1_bH9GG1br6-XmYsBFuJAgCLcB/s1600/cara7.jpg" /></a></div>
<br />
<br />
silahkan anda ganti <b>http://www.url-yang-di-blokir.com</b> pada code diatas dengan URL web/blog anda yang diblokir. bagian lainnya ga wajib diganti tapi lebih baik diganti.<br />
<br />
selesai. silahkan anda test dengan mengunjungi URL blog anda kalo dialihkan ke situs tujuan berati semua sudah benar. tinggal anda share di FB saja.<br />
<br />
memang cara ini agak ribet dan kurang efektif karena kita tidak bisa sesuka hati mengganti linknya. satu alamat blog cuman buat satu URL saja.<br />
<br />
<b>Cara ketiga buka Blokir link / URL di Facebook dengan Safe Link&nbsp;</b><br />
<span style="color: red;"><br /></span>
cara ketiga ini paling simple penerapannya dibanding kedua cara diatas. tapi agak berbelit-belit nantinya karena yang mau berkunjung ke link aslinya harus menunggu beberapa detik sampai link berhasil di encode/decrypt. dan juga tidak semua perangkat mendukung cara ini. contohnya perangkat mobile dengan browser opramini.<br />
<br />
pertama, kalo mau ribet cari di google "free safe link converter" atau "safe link generator" ada banyak sekali situs yang melayani safe link gratisan. kalo ga mau ribet pake yang buat &nbsp;contoh safe link punya kompiajaib.com dibawah ini<br />
<br />
pertama copy URL terblokir yang mau anda share ke Facebook<br />
<br />
lalu kunjungi situsnya <b><a href="http://safelink-3.blogspot.com/">http://safelink-3.blogspot.com</a></b><br />
<br />
masukan URL yang anda copy tadi ke kolom URL dengan cara klik kolom URL lalu tekan&nbsp;<b style="color: blue;">Control&nbsp;+ V</b>&nbsp;pada keyboard (ga bisa pake klik kanan mouse soalnya). lanjuktan dengan klik tombol <b><span style="color: blue;">safe Link Now</span></b><br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://3.bp.blogspot.com/-geTFcb-v__w/WPOQSmBKT4I/AAAAAAAAAaA/E4CZ0gV2kEIHbx6T8w_SgxVQX6WHzwMOACLcB/s1600/carasafelink1.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://3.bp.blogspot.com/-geTFcb-v__w/WPOQSmBKT4I/AAAAAAAAAaA/E4CZ0gV2kEIHbx6T8w_SgxVQX6WHzwMOACLcB/s1600/carasafelink1.jpg" /></a></div>
<br />
tunggu sejenak URL anda sedang diproses. kalo sudah selesai, anda copy URL dengan cara klik kolom URL dan tekan kombinasi <b><span style="color: blue;">Control&nbsp;+ C&nbsp;</span></b>atau bisa juga dengan klik tombol <span style="color: blue;"><b>Copy your link to clipboard</b></span>.<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://2.bp.blogspot.com/-n0Yt369sllo/WPOQRllDUYI/AAAAAAAAAZ8/wJX6-2MU-rUz1eCF1uJPom9BWNo3HzVcQCLcB/s1600/carasafelink2.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-n0Yt369sllo/WPOQRllDUYI/AAAAAAAAAZ8/wJX6-2MU-rUz1eCF1uJPom9BWNo3HzVcQCLcB/s1600/carasafelink2.jpg" /></a></div>
<br />
<br />
contoh link jadinya seperti ini<br />
<span style="color: blue;"><br /></span>
<span style="color: blue;"><a href="https://safelink-3.blogspot.com/2016/09/how-to-read-forex-chart-properly.html?url=aHR0cDovL2t5YXRhYmxvZy5ibG9nc3BvdC5jb20=">https://safelink-3.blogspot.com/2016/09/how-to-read-forex-chart-properly.html?url=aHR0cDovL2t5YXRhYmxvZy5ibG9nc3BvdC5jb20=</a></span><br />
<span style="color: blue;"><br /></span>
annoying banget kan kalo share link seperti itu di Facebook? emang sich.. tapi daripada kagak bisa share sama sekali. hehe<br />
<br />
makanya saya saranin pake cara yang pertama karena ga terlalu annoying, pasti suport semua browser, ga ribet juga plus dapet backlink permanen gratis.<br />
<br />
<br />
<br />
<br />
<br />
<br />

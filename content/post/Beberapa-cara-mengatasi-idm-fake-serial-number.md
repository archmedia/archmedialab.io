---
title: "Beberapa Cara Mengatasi Idm Fake Serial Number"
post_highlight: ""
author: "KyataMedia" 
date: 2017-04-10T06:04:17Z
PublishDate: 2017-04-10T06:04:17Z
Lastmod: 2017-04-10T06:04:17Z
slug: "beberapa-cara-mengatasi-idm-fake-serial-number"
categories: [Guide]
tags: [Tutorial, Panduan, Software, Tips dan Trick, Internet]
cover: "https://i.ytimg.com/vi/Dn7W0axsxQ4/maxresdefault.jpg"
draft: false
---

<b>Beberapa cara mengatasi idm fake serial number</b> -, IDM&nbsp;adalah satu dari beberapa 

<a href="/" target="_blank">software download manager</a>&nbsp; powerfull dan wajib terinstal di PC bagi kebanyakan orang. namun terkadang ada masalah yang sangat mengganggu yaitu berupa pesan pop up seperti 

<b>
<a href="https://www.google.co.id/search?q=IDM+has+been+registered+with+the+Fake+serial+number&amp;oq=IDM+has+been+registered+with+the+Fake+serial+number" target="_blank">IDM has been registered with the Fake serial number</a></b>". 

oke, pesan tersebut jelas sekali maksudnya, karena serial yang anda gunakan adalah fake. akan tetapi pada kenyataannya ada banyak pengguna yang mengeluhkan bahwa serial yang gunakan adalah murni asli dan legal namun pop up tersebut selalu muncul karena ada kesalahan system validasi dari IDM-nya.<br />
<br />

<a href="https://i.ytimg.com/vi/Dn7W0axsxQ4/maxresdefault.jpg" imageanchor="1"    ><img alt="IDM has been registered with the Fake serial number" border="0" height="180" src="https://i.ytimg.com/vi/Dn7W0axsxQ4/maxresdefault.jpg" title="IDM has been registered with the Fake serial number" width="320" /></a>

source: i.ytimg.com
<br />
bagi anda yang mengalami masalah yang sama silahkan dicoba beberapa metode yang ada di artikel ini. semoga ada yang bisa jadi solusi masalah IDM anda.<br />
<br />
Sebelum anda melakukan salah satu metode dibawah pastikan IDM anda uninstall terlebih dahulu dengan mennggunakan Revo Uninstaller yang bisa anda download disini <a href="http://www.revouninstaller.com/revo_uninstaller_free_download.html">www.revouninstaller.com/revo_uninstaller_free_download.html</a>. tujuannya agar PC atau laptop anda benar-benar bersih dari sisa-sisa data dan file instalan IDM. perlu diketahui, jika anda uninstal pake sofware bawaan windows maka sekalipun anda restar dan instal ulang IDM tetap saja akan muncul pesan error seperti fake serial number karena IDM sengaja menyisakan data di PC anda.<br />
<br />
setelah anda uninstal IDM dengan revo uninstaller silahkan restart PC. lalu instal kembali IDMnya. perlu diperhatikan, jangan dulu anda koneksikan PC anda ke internet, agar IDM tidak melakukan validasi dan memblokir IDM anda kembali.<br />
<br />
<b>Metode pertama</b><br />
<br />
pada metode pertama cara mengatasi idm fake serial number yang berupa Pop-up yaitu dengan menggunakan trik tweak pada registry.<br />
<br />
caranya tekan tombol Windows + R untuk menjalankan Dialog box<br />
lalu ketik "regedit" lalu tekan tombol enter<br />
klik pada HKEY_CURRENT_USER &gt; Software &gt; DownloadManager &gt;<br />
cari registry dengan nama "CheckUpdtVM"<br />
silahkan ubah value data dari angka 1 menjadi 0<br />
<br />
silahkan restar komputer anda lalu coba buka kembali IDM-nya.<br />
<br />
<b>Metode kedua</b><br />
<br />
pada metode kedua ini dengan cara menghapus file yang ada di folder program file yang ada di folder IDM. silahkan buka windows explore lalu masuk ke "C:\Program Files (x86)\Internet Download Manager" atau bagi windows XP user masuk ke "C:\Program Files\Internet Download Manager" lalu hapus file dengan nama &nbsp;"IDMGrHlp.exe"<br />
<br />

<a href="https://uglyduckblog.files.wordpress.com/2013/11/idmreg1.jpg?w=470&amp;h=229" imageanchor="1"    ><img alt="IDMGrHlp.exe" border="0" height="155" src="https://uglyduckblog.files.wordpress.com/2013/11/idmreg1.jpg?w=470&amp;h=229" title="IDMGrHlp.exe" width="320" /></a>
<br />

source: uglyduckblog.files.wordpress.com
<br />
sekarang buka IDMnya dan pop up pun sudah tidak muncul lagi.<br />
<br />
jika muncul "IDM cannot find 1 files that are necessary for browser and system integration. Please reinstall IDM" silahkan anda buat satu file notepad dengan cara klik kanan mouse &gt; New &gt; Text Doxcument lalu rename nama dan ekstensi file dari yang awalnya Text Document.txt ubah menjadi &nbsp;"IDMGrHlp.exe"<br />
<br />
silahkan buka kembali IDM nya dan pesan tersebut tidak akan muncul lagi.<br />
<br />
<b>Metode ketiga</b><br />
<br />
pada metode ketiga ini dengan cara memblokir server internet yang digunakan oleh <a href="https://www.internetdownloadmanager.com/" target="_blank">Internet Download Manager</a> (IDM) untuk mengecek update terbaru maupun memvalidasi serial number yang anda gunakan. dengan cara ini IDM tidak akan bisa terhubung ke server alhasil IDM tidak bisa melakukan pengecekan apakah serial nomor IDM anda itu ilegal atau IDM anda adalah versi crack atau hsil patching.<br />
<br />
caranya masuk ke direktori "C:\Windows\System32\drivers\etc" lalu klik kanan pada file host lalu open with Notepad. kalo sudah silahkan copy dan paste kan ini di bagian paling bawah<br />
<br />
127.0.0.1 tonec.com<br />
127.0.0.1 www.tonec.com<br />
127.0.0.1 registeridm.com<br />
127.0.0.1 www.registeridm.com<br />
127.0.0.1 secure.registeridm.com<br />
127.0.0.1 internetdownloadmanager.com<br />
127.0.0.1 www.internetdownloadmanager.com<br />
127.0.0.1 secure.internetdownloadmanager.com<br />
127.0.0.1 mirror.internetdownloadmanager.com<br />
127.0.0.1 mirror2.internetdownloadmanager.com<br />
127.0.0.1 mirror3.internetdownloadmanager.com<br />
<br />
kurang lebih jadi seperti ini<br />
<br />
<br />

<a href="http://3.bp.blogspot.com/-hepRlBlNgqw/UN7Yi6em1HI/AAAAAAAAAAM/iBUBuWXIzQY/s1600/IDM+registration+keys.jpg" imageanchor="1"    ><img alt="Internet Download Manager (IDM) host etc tweak" border="0" src="http://3.bp.blogspot.com/-hepRlBlNgqw/UN7Yi6em1HI/AAAAAAAAAAM/iBUBuWXIzQY/s1600/IDM+registration+keys.jpg" height="214" title="Internet Download Manager (IDM) host etc tweak" width="320" /></a>

source: 3.bp.blogspot.com
<br />
<br />
.# Copyright (c) 1993-2009 Microsoft Corp.<br />
.#<br />
.# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.<br />
.#<br />
.# This file contains the mappings of IP addresses to host names. Each<br />
.# entry should be kept on an individual line. The IP address should<br />
.# be placed in the first column followed by the corresponding host name.<br />
.# The IP address and the host name should be separated by at least one<br />
.# space.<br />
.#<br />
.# Additionally, comments (such as these) may be inserted on individual<br />
.# lines or following the machine name denoted by a '.#' symbol.<br />
.#<br />
.# For example:<br />
.#<br />
.# &nbsp; &nbsp; &nbsp;102.54.94.97 &nbsp; &nbsp; rhino.acme.com &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;.# source server<br />
.# &nbsp; &nbsp; &nbsp; 38.25.63.10 &nbsp; &nbsp; x.acme.com &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;.# x client host<br />
<br />
127.0.0.1 tonec.com<br />
127.0.0.1 www.tonec.com<br />
127.0.0.1 registeridm.com<br />
127.0.0.1 www.registeridm.com<br />
127.0.0.1 secure.registeridm.com<br />
127.0.0.1 internetdownloadmanager.com<br />
127.0.0.1 www.internetdownloadmanager.com<br />
127.0.0.1 secure.internetdownloadmanager.com<br />
127.0.0.1 mirror.internetdownloadmanager.com<br />
127.0.0.1 mirror2.internetdownloadmanager.com<br />
127.0.0.1 mirror3.internetdownloadmanager.com<br />
<br />
terahir silahkan simpan.<br />
<br />
bagi yang masih gagal atu ada yang perlu ditanyakan jangan ragu untuk ditanyakan di kolom komentar.<br />
<br />
<br />
<br />
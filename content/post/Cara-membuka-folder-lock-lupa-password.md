---
title: "Cara Membuka Folder Lock Lupa Password"
post_highlight: ""
author: "KyataMedia" 
date: 2017-05-28T08:04:17Z
PublishDate: 2017-09-28T05T06:04:17Z
Lastmod: 2017-05-28T08:04:17Z
slug: "cara-membuka-folder-lock-lupa-password"
categories: [Guide]
tags: [Software, Windows, Tips dan Trick, Desktop]
cover: ""
draft: false
---

<a href="https://1.bp.blogspot.com/47bPB9ZFTbU99uXiEkLoMk90bJDaQkOn81J1bGAYARb3nUKeS1Cbu1i0hgfOj1pz98bo=w300" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img alt="Cara Membuka File Di Folder Lock 7 Yang Lupa Password" border="0" data-original-height="300" data-original-width="300" src="https://1.bp.blogspot.com/47bPB9ZFTbU99uXiEkLoMk90bJDaQkOn81J1bGAYARb3nUKeS1Cbu1i0hgfOj1pz98bo=w300" title="Cara Membuka File Di Folder Lock 7 Yang Lupa Password" /></a><br />
<b>Lupa Password Folder Lock Anda?</b>,- &nbsp;Folder lock memang sangat bermanfaat sekali untuk mengamankan file atau folder pribadi anda yang tidak ingin dibuka atau diketahui orang lain. &nbsp;terutama bagi anda yang menggunakan PC umum misal di kantor atau di sekolahan yang dipakai oleh banyak orang. dengan Folder lock anda masih tetap bisa menyimpan file pribadi tanpa takut diakses atau dibuka oleh orang lain.<br />
<br />
Akan tetapi software folder lock juga bisa jadi senjata makan tuan jika anda lupa password dan anda tidak tau bagaimana harus mereset atau merubah password anda. menginstal ulang dan uninstal ulang juga tidak membantu karena Folder Lock tetap saja meminta password.<br />
<br />
penulis sendiri sekitar 3 hari yang lalu mengalami lupa password Folder Lock. penulis mencoba uninstal ternyata tidak bisa karena harus memasukan password terlebih dahulu. kedua penulis mencoba downgrade ke versi lama tetap saja tidak memecahkan masalah, folder lock tetap meminta password dari versi yang terbaru,<br />
<br />
penulis bahkan mencoba uninstal paksa dengan menghapus semua file terkait yang ada di&nbsp;<b><span style="color: blue;">C:\Program Files\NewSoftware's\Folder Lock</span></b> kemudian instal ulang tapi tetap saja tidak berhasil. software ini menggunakan berbagai file pengaman yang dimasukan di folder system windows.<br />
<br />
karena penulis merasa males kalo harus menghapus semua file system yang terkait dengan Folder lock maka penulis memutuskan untuk mencari jalan keluar lain dengan melakukan googling dengan kata kunci "<b><span style="color: blue;">Cara membuka folder lock lupa password</span></b>" dan penuli dapatkan trik yang sangat mudah sekali.<br />
<br />
<b>Cara Mudah Reset Password Program Folder Lock</b><br />
<b><br /></b>
oh ya, penulis menggunakan <b><span style="color: blue;">Folder lock versi 7.5</span></b>&nbsp; namun dari info yang penulis dapat katanya sich cara ini bisa digunakan untuk semua versi Folder lock.<br />
<br />
- pertama, anda buka folder locknya<br />
- kedua, anda cukup masukan saja&nbsp;<b><span style="color: blue;">Serial Key</span></b> folder lock anda yang digunakan untuk registari Folder lock anda kedalam kolom <b><span style="color: blue;">Master Password</span></b><br />
<br />
<table align="center" cellpadding="0" cellspacing="0" class="tr-caption-container" style="margin-left: auto; margin-right: auto; text-align: center;"><tbody>
<tr><td style="text-align: center;"><a href="https://2.bp.blogspot.com/-WG8Z9b_SzHk/WSmx7z8YeKI/AAAAAAAAAx8/URw84_sf-7UheoshDlCdGAxi0e4MI--7gCLcB/s1600/Untitled.jpg" imageanchor="1" style="margin-left: auto; margin-right: auto;"><img alt="Folder Lock 7 Recovery - Cara Mereset Password" border="0" data-original-height="639" data-original-width="927" height="275" src="https://2.bp.blogspot.com/-WG8Z9b_SzHk/WSmx7z8YeKI/AAAAAAAAAx8/URw84_sf-7UheoshDlCdGAxi0e4MI--7gCLcB/s400/Untitled.jpg" title="Folder Lock 7 Recovery - Cara Mereset Password" width="400" /></a></td></tr>
<tr><td class="tr-caption" style="text-align: center;">Cara Reset password Folder Lock dengan Serial Key</td></tr>
</tbody></table>
<span id="goog_31933221"></span><span id="goog_31933222"></span><br />
<br />
contoh&nbsp;Serial Key folder lock saya seperti ini:&nbsp;<b><span style="color: blue;">F7-85957643-9-396875</span></b><br />
<br />
kemudian klik tombol <span style="color: blue;"><b>OK</b></span> dan Folder lock sudah bisa dibuka.<br />
<br />
tinggal anda setting ulang Master Password Folder lock anda di menu <span style="color: blue;"><b>setting &gt; Password Securit</b>y</span>.<br />
<br />
<br />
<br />
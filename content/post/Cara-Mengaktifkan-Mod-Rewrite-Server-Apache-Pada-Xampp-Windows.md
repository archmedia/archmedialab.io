---
title: "Cara Mengaktifkan Mod Rewrite Server Apache Pada Xampp Windows"
post_highlight: ""
author: "KyataMedia" 
date: 2012-10-13T11:01:00-07:00
PublishDate: 2012-10-13T11:01:00-07:00
Lastmod: 2012-10-13T11:01:00-07:00
slug: "cara-mengaktifkan-mod-rewrite-server-apache-pada-x"
categories: [Guide]
tags: [Tutorial Wordpress.org]
cover: ""
draft: false
---

<a href="http://www.google.com/" target="_blank"><b>Cara Membuat Blog WordPress.org</b></a>,  <a href="http://www.google.com/2012/10/cara-mengaktifkan-mod-rewrite-server.html" target="_blank"><b>Cara Mengaktifkan Mod Rewrite Server Apache Pada  Xampp Windows</b></a>
 –  Mod Rewrite atau Modul Rewrite Apache pada paket XAMPP secara 
default tidak diaktifkan.  Padahal dengan Mod Rewrite, kita bias merubah
  struktur permalink yang panjang menjadi pendek.<br />
<br />
Contoh : <b>www.google.com/folder1/folder2/ post=1.php</b> menjadi <b>www.google.com/judul-artikel.php&nbsp;</b><br />
<b>&nbsp;</b>
	<br />
pada permalink yang pertama terkesan awut-awutan dan sulit di ingat, 
dan mesin pencari bakalan lebih sulit mengenali karena pada permalink 
tidak tersisip kata kunci yang berhubungan dengan artikel (tidak SEO 
friendly). sedangkan yang kedua simple, clean, dan mudah di ingat, 
selain itu search engine seperti google.com dan bing.com juga lebih 
mudah mengindex dan mengenali isi dari artikel anda, karena pada 
permalink  mengandung kata kunci yang sama dengan judul artikel 
(permalink SEO friendly).
<br />
<br />
Selain untuk mengubah struktur permalink, mod rewrite juga bisa digunakan untuk merubah nama domain dari www menjadi non www.<br />
<br />
contoh : <b>http://www.google.com</b> menjadi <b>http://google.com</b><br />
<b>&nbsp;</b>
	<br />
atau sebaliknya  untuk merubah nama domain dari non www menjadi www.  dan masih banyak lagi fungsi fungsi lain.
<br />
<br />
Sebenarnya sudah banyak blog yang mebahas mengenai <a href="http://www.google.com/2012/10/cara-mengaktifkan-mod-rewrite-server.html" target="_blank"><b>Cara Mengaktifkan Mod Rewrite Pada Xampp Server</b></a>.
 Akan tetapi kebanyakan tidak disertai dengan gambar. Bagi sebagian 
orang, penjelasan tidak cukup hanya dengan melalui teks saja, oleh 
karena itu, pada postingan ini saya akan menjelaskan dengan disertai 
gambar agar lebih mudah dipahami.
<br />
<br />
berikut langkah-langkahnya :
<br />
<br />
<ul>
<li>Buka <b>Windows Explorer</b>. Kemudian buka direktori tempat Anda menginstal XAMPP. ( Katakanlah pada drive <b>C</b>. )


</li>
</ul>
<div style="text-align: center;">
<img alt="" src="http://hayinraditya.files.wordpress.com/2012/10/101312_1938_caramengakt1.jpg?w=490" />
	</div>
<div style="text-align: center;">
</div>
<ol>
</ol>
<ul>
<li>Cari dan double Klik folder <b>xampp.</b></li>
</ul>
<ol>
</ol>
<br />
<div style="text-align: center;">
<img alt="" src="http://hayinraditya.files.wordpress.com/2012/10/101312_1938_caramengakt2.jpg?w=490" />
	</div>
<div style="text-align: center;">
</div>
<ol>
</ol>
<ul>
<li>
<div>
Selanjutnya Double Klik pada folder <b>apache.</b>
			</div>
<br />

</li>
</ul>
<ol>
</ol>
<div style="text-align: center;">
<img alt="" src="http://hayinraditya.files.wordpress.com/2012/10/101312_1938_caramengakt3.jpg?w=490" />
	</div>
<div style="text-align: center;">
</div>
<ol>
</ol>
<ul>
<li>
<div>
Double Klik folder <b>conf.</b>
			</div>
<br />

</li>
</ul>
<ol>
</ol>
<div style="text-align: center;">
<img alt="" src="http://hayinraditya.files.wordpress.com/2012/10/101312_1938_caramengakt4.jpg?w=490" />
	</div>
<div style="text-align: center;">
</div>
<ol>
</ol>
<ul>
<li><b>Klik kanan</b>  pada file <b>httpd.conf.</b>
		</li>
</ul>
<ol>
</ol>
<div style="text-align: center;">
</div>
<div style="text-align: center;">
<img alt="" src="http://hayinraditya.files.wordpress.com/2012/10/101312_1938_caramengakt5.jpg?w=490" />
	</div>
<ol>
</ol>
<ul>
<li>Pilih <b>Open With…</b>
		</li>
</ul>
<ol>
</ol>
<div style="text-align: center;">
</div>
<div style="text-align: center;">
<img alt="" src="http://hayinraditya.files.wordpress.com/2012/10/101312_1938_caramengakt6.jpg?w=490" />
	</div>
<ol>
</ol>
<ul>
<li>Pilih <b>Wordpad</b>, kemudian klik tombol <b>OK</b>.
</li>
</ul>
<ol>
</ol>
<br />
<div style="text-align: center;">
<img alt="" src="http://hayinraditya.files.wordpress.com/2012/10/101312_1938_caramengakt7.jpg?w=490" />
	</div>
<div style="text-align: center;">
</div>
<ol>
</ol>
<ul>
<li>Setelah jendela Wordpad terbuka, Anda cari “<b>LoadModule rewrite_module modules/mod_rewrite.so</b>” tanpa tanda kutip. Atau biar lebih mudah, silahkan  Tekan tombol  kombinasi  <b>CTRL+F</b> pada keyboard, Kemudian Anda <b>copy</b> teks yang saya bold dan beri warna merah di bawah ini ;
</li>
</ul>
<ol>
</ol>
<div style="text-align: center;">
</div>
<div style="text-align: center;">
<span style="color: red;"><b>LoadModule rewrite_module modules/mod_rewrite.so<br />
</b></span></div>
<br />
<div style="margin-left: 36pt;">
lalu <b>paste</b> kan <span style="font-family: Courier New; font-size: 10pt;">pada kolom <b>Find what</b>, dilanjutkan dengan menekan tombol <b>Find Next.<br />
</b></span></div>
<div style="margin-left: 36pt;">
</div>
<div style="text-align: center;">
<img alt="" src="http://hayinraditya.files.wordpress.com/2012/10/101312_1938_caramengakt8.jpg?w=490" />
	</div>
<div style="text-align: center;">
</div>
<ol>
</ol>
<ul>
<li>
<div>
Jika sudah  ketemu, Hapus tanda <b>#</b> (pagar) di depan <b>LoadModule rewrite_module modules/mod_rewrite.so</b>
			</div>
<br />

</li>
</ul>
<ol>
</ol>
<div style="text-align: center;">
<img alt="" src="http://hayinraditya.files.wordpress.com/2012/10/101312_1938_caramengakt9.jpg?w=490" />
	</div>
<div style="text-align: center;">
</div>
<div style="margin-left: 36pt;">
Menjadi seperti di bawah ini.
</div>
<div style="margin-left: 36pt;">
</div>
<div style="margin-left: 36pt; text-align: center;">
<img alt="" src="http://hayinraditya.files.wordpress.com/2012/10/101312_1938_caramengakt10.jpg?w=490" />
	</div>
<div style="margin-left: 36pt; text-align: center;">
</div>
<ol>
</ol>
<ul>
<li>terahir silahkan Anda <b>Save httpd.conf</b> yang tadi sudah diedit.
</li>
</ul>
<ol>
</ol>
<br />
Sekarang Mod Rewrite Pada Xampp Server Anda telah diaktifkan.
<br />
<br />
Demikian tadi<b> <a href="http://www.google.com/2012/10/cara-mengaktifkan-mod-rewrite-server.html" target="_blank">Cara Mengaktifkan Mod Rewrite Server Apache Pada  Xampp Windows</a> </b>dari<b> <a href="http://www.google.com/" target="_blank">Cara Membuat Blog WordPress.org</a></b>. moga bias bermanfaat….

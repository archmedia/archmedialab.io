---
title: "Gadis Ini Lebih Cerdas Dari Einstein"
post_highlight: ""
author: "MobaGenie" 
date: 2011-03-22T00:25:00Z
PublishDate: 2011-03-22T00:25:00Z
Lastmod: 2011-03-22T19:07:26Z
slug: "gadis-ini-lebih-cerdas-dari-einstein"
categories: []
tags: []
cover: "http://2.bp.blogspot.com/-4zkIsuJ6K7o/TYeJp7RFIWI/AAAAAAAAACE/s5ukxEAcfiE/s1600/gadis%2Bcerdas.jpg"
draft: false
---

<div class="separator" style="clear: both; text-align: center;"><a href="http://2.bp.blogspot.com/-4zkIsuJ6K7o/TYeJp7RFIWI/AAAAAAAAACE/s5ukxEAcfiE/s1600/gadis%2Bcerdas.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="218" src="http://2.bp.blogspot.com/-4zkIsuJ6K7o/TYeJp7RFIWI/AAAAAAAAACE/s5ukxEAcfiE/s320/gadis%2Bcerdas.jpg" width="274" /></a></div>Victoria Cowie, 11 tahun, seperti anak seusianya. Dia suka berenang dan mendengarkan musik, yang membedakan dia memiliki tingkat kecerdasan (IQ) 162, lebih tinggi dari Albert Einstein, Stephen Hawking dan Bill Gates yang memiliki tingkat kecerdasan 160.<br />
<!--more--><br />
<br />
"Ketika menerima hasilnya, aku sungguh terkejut," kata Victoria seperti dikutip dari Daily Mail, Selasa (15/3). "Meski senang dianggap cerdas, aku sungguh terbebani bila dibandingkan dengan para pemikir besar," katanya.<br />
<br />
Victoria melakukan tes IQ sebagai syarat untuk bergabung dalam kelompok Mensa. Hanya mereka memiliki kepintaran di atas rata-rata yang dapat bergabung di kelompok ini, yakni di atas 148. Di Inggris, hanya sekitar 1 persen anak yang memiliki tingkat IQ sangat tinggi.<br />
<br />
Dengan hasil ini, Victoria melewati beberapa nama besar yang dikenal memiliki IQ super. Selain Einstein, Gates dan Hawking, Victoria juga melewati Sigmund Freud yang memiliki IQ 156, Napoleon Bonaparte yang memiliki IQ 145 dan Hillary Clinton memiliki IQ 140.<br />
<br />
Dalam kesehariannya, Victoria yang tinggal di Wolverhampton, Inggris, sangat suka dengan puzzle dan semua hal yang berkaitan dengan pemecahan suatu masalah. Victoria juga senang menari dan bermain musik, seperti piano, cello dan saxophone. Dia memendam cita-cita ingin mengambil studi bidang sains, terutama biologi.<br />
<br />
Ibu Victoria, Alison, 44, sangat bangga dengan anaknya. "Kami tahu dia anak yang cerdas, tapi kami tak menyangka dia akan bergabung di Mensa," katanya. Begitu juga dengan ayahnya, David, 42. "Kami berdua tidak ada yang bergabung dengan Mensa," kata David. "Maka ketika dia masuk kami terkejut dan bangga."
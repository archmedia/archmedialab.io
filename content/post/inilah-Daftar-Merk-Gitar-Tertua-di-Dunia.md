---
title: "Inilah Daftar Merk Gitar Tertua Di Dunia"
post_highlight: ""
author: "KyataMedia" 
date: "2017-03-31T05:54:55Z" 
PublishDate: "2017-03-31T05:54:55Z" 
Lastmod: "2017-03-31T05:54:55Z" 
slug: "inilah-daftar-merk-gitar-tertua-di-dunia"
categories: [Music]
tags: [Music, Gitar, Sejarah]
cover: "https://kyatamedia.gitlab.io/blog/static/img/200px-Fender_guitars_logo.svg.png"
draft: false
---

<div dir="ltr" style="text-align: left;" trbidi="on">
<b>inilah Daftar Merk Gitar Tertua di Dunia</b>, - Di dunia ini ada banyak sekali gitaris dengan skill dan talenta yang mengagumkan. Tapi sehebat apapun gitaris pasti membutuhkan gitar yang bagus juga.<br />
<br />
sama seperti gitaris, ada yang jadi legendaris gitar pun ada yang jadi legendaris juga. beberapa gitar tidak hanya legendaris tapi juga jadi merk gitar tertua di Dunia.<br />
<br />
penasaran merk atau perusahaan gitar apa sajakah itu? berikut ini daftar lengkapnya.<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Fender_guitars_logo.svg/200px-Fender_guitars_logo.svg.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://kyatamedia.gitlab.io/blog/static/img/200px-Fender_guitars_logo.svg.png" /></a></div>
<br />
<br />
<b>1946 - Fender</b><br />
<br />
Siapa yang tidak kenal merk gitar satu ini? saya yakin sekalipun anda bukanlah seorang musisi atau lebih spesifik lagi seorang gitaris sekalipun saya yakin anda pasti<br />
<br />
kenal dengan merk yang satu ini.<br />
<br />
Fender Musical Instruments Corporation atau lebih dikenal dengan Fender, adalah pabrikan alat musik dawai dan pengeras seperti gitar elektrik. Beberapa produk<br />
<br />
terkenal mereka antara lain termasuk Fender Telecaster, Fender Stratocaster, dan Fender Precision Bass.<br />
<br />
tapi tahukah anda kalau Fender adalah termasuk salah satu merk gitar tertua di dunia? Fender sudah berdiri sejak Tahun 1946 dengan nama "Fender Electric<br />
<br />
Instrument Manufacturing Company" oleh seorang ahli teksini elektronik bernama Leo Fender. coba disini siapa yang lahirnya Tahun 1946? hehe<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Gibson_logo.svg/200px-Gibson_logo.svg.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Gibson_logo.svg/200px-Gibson_logo.svg.png" /></a></div>
<br />
<br />
<b>1902 - Gibson</b><br />
<br />
Anda gitaris atau musisi? jangan bilang anda tidak kenal merk Gibson :D. yap, Gibson adalah merk gitar mainstream sama seperti Fender. Gibson Brand, Inc adalah<br />
<br />
merk gitar yang bermarkas di Nashville, Tennesse Amerika Serikat. &nbsp;awalnya perusahaan ini menamai dirinya Gobson Guitar Corp akan tetapi pada 11 Juni 2013<br />
<br />
perusahaan ini mengganti namanya dengan Gibson Brand, Inc.<br />
<br />
Oville Gibson, adalah satu-satunya orang yang bertanggungjawab atas berdirinya &nbsp;"The Gibson Mandolin-Guitar Mfg. Co., Ltd." pada Tahun 1902 yang sejatinya<br />
<br />
adalah cikal bakal Gibson Brand, Inc. coba hitung 1902 sampai 2017, berapa tahun? yap.. &nbsp;kurang lebih 115 Tahun! ayo siapa pembaca disini yang udah berumur<br />
<br />
115 Tahun acungkan jari? hihi<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://upload.wikimedia.org/wikipedia/id/thumb/b/bd/Yamaha.png/200px-Yamaha.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://upload.wikimedia.org/wikipedia/id/thumb/b/bd/Yamaha.png/200px-Yamaha.png" /></a></div>
<br />
<br />
<b>1887 - Yamaha</b><br />
<br />
jangan sekali kali anda ngaku orang Indonesia kalo anda tidak kenal dengan merk yang satu ini. gimana ga kenal, hampir setiap hari orang Indonesia selalu akrab<br />
<br />
dengan yang namanya Yamaha. dari mulai kendaraan, alat musik, sampai peralatan rumah tangga dan masih banyak lagi yang biasa kita gunakan sehari hari.<br />
<br />
tapi taukah anda Yamaha Corporation, dengan nama awal Nippon Gakki CO, Ltd oleh sang pendiri bernama Torakusu Yamaha adalah perusahaan Jepang yang<br />
<br />
sudah berdiri sejak Tahun 1887? &nbsp;wew.. tua banget..<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Washburn_HB35_ts.JPG/170px-Washburn_HB35_ts.JPG" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Washburn_HB35_ts.JPG/170px-Washburn_HB35_ts.JPG" /></a></div>
<br />
<b>1883 - Washburn</b><br />
<br />
Yamah, Gibson dan juga Fender boleh saja sombong dengan popularitas mereka sebagai sebuah merk gitar. tapi sejarah membuktikan bahwa Washburn adalah merk<br />
<br />
gitar yang lebih tua dari mereka. ibarat manusia, Washburn sudah bisa merangkak merek bertiga masih belom pada lahir. hehe<br />
<br />
entah saking tuanya atau saking ga populernya sampai saya cari infonya sangat sedikit sekali. barangkali disini pembaca pun 90% pada baru denger ada merk gitar<br />
<br />
ini? ayo tunjuk jari .. :D<br />
<br />
Washburn Guitars adalah perusahaan gitar asal Amerika Serikat atau lebih tepatnya di Chicago, Illionis yang didirikan sudah dari sejak Tahun 1883. nah loh.. udah<br />
<br />
embah banget kan..<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Rickenbacker_logo.svg/250px-Rickenbacker_logo.svg.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Rickenbacker_logo.svg/250px-Rickenbacker_logo.svg.png" /></a></div>
<br />
<br />
<b>1932 - Rickenbacker</b><br />
<br />
Sepertinya bukan termasuk merk gitar yang populer sama seperti Washburn tapi untuk urusan tua jangan ditanya lagi.<br />
<br />
Rickenbacker International Corporation, atau juga dikenal dengan Rickenbacker, perusahaan manufaktur alat musik electric yang berbasis di Santa Ana, California ini<br />
<br />
sudah didirikan sejak Tahun 1932 oleh Adolph Rickenbacher dan George Beauchamp sebagai perusahaan electric gitar dan juga gitar Hawai.<br />
<br />
bahkan di salah satu artikel menyebutkan kalau perusahaan ini adalah perusahaan pertama di dunia yangt memproduksi gitar, gitar listrik dan juga gitar bass. entah<br />
<br />
yang bener yang mana. tapi yang jelas julukan mastah atau embah-nya merk gitar adalah tidak berlebihan. hehe<br />
<br />
<br />
<div class="separator" style="clear: both; text-align: center;">
<a href="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Martin_guitar_logo.png/250px-Martin_guitar_logo.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Martin_guitar_logo.png/250px-Martin_guitar_logo.png" /></a></div>
<br />
<br />
<b>1833 - CF Martin &amp; Co</b><br />
<br />
nah yang ini nih bisa dibilang nenek moyangnya merk gitar. sudah bukan embah lagi. sebelum mereka-mereka embah, anak, cucu dan cicit masih belom ada atau pas<br />
<br />
mereka masih pada suka &nbsp;ngompol C. F. Martin &amp; Company ini sudah gede bahkan sudah dewasa. hehe<br />
<br />
C. F. Martin &amp; Company adalah perusahaan Gitar akustik atau biasa disebut gitar kemprung oleh orang Indonesia ini awalnya didirikan dan berlokasi di New York<br />
<br />
City, New York United States oleh Christian Frederick Martin.<br />
<br />
CF Martin &amp; Co sudah didirikan dari Tahun 1833 yang sekaligus mencatatkan sejarah sebagai Perusahaan Gitar tertua di Dunia! gimana tidak jadi tetua orang sudah<br />
<br />
184 Tahun kalo dihitung dari sekarang..<br />
<br />
dari daftar diatas adakah merk gitar yang juga jadi merk gitar favorite anda atau bahkan ada merk yang lebih tua dari merk gitar diatas silahkan berikan unek-unek<br />
<br />
anda di kolom komentar. :)<br />
<br />
<br />
sumber artikel dan gambar : wikipedia.org<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br /></div>

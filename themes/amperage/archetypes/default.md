+++
#draft = true
#date = "{{ .Date }}"
#publishdate = "{{ .Date }}"
date: {{ .Date }}
PublishDate: {{ .Date }}
Lastmod: {{ .Date }}
draft: true
slug: "{{substr ( lower .File.BaseFileName  | replaceRE "^[0-9]{14}-" "" | replaceRE "," "") 0 50 }}"

title = "{{ replace .Name "-" " " | title }}"

description = ""

summary = ""

tags = []

keywords = []

[amp]
    elements = []

[author]
    name = ""
    homepage = ""

[image]
    src = ""

[ogp]
    title = ""
    url = ""
    description = ""
    image = ""
    site = ""

[twitter]
    title = ""
    url = ""
    description = ""
    image = ""
    site = ""

[sitemap]
    changefreq = "monthly"
    priority = 0.5
    filename = "sitemap.xml"

+++

